#include <stdio.h>  // all we need 

/*

"""
C Language
Apple LLVM version 6.0 (clang-600.0.56) (based on LLVM 3.5svn)

This Source will Parse your Integer (1-9999) into Word e.g ( 9143 to "Nine Thousand One Hundred Fourty Three")
Made this for: Dyuwensi's Thesis
"""
*/


void OneIntegral(int); // 3
void TwoIntegral(int); // 33
void ThreeIntegral(int); // 333

int main()
{
    // Initialize {
    char *MyStringList[] = {"Zero","One","Two","Three","Four","Five","Six","Seven","Eight","Nine","Ten"};
    int UserInput = 1;
    int TwoInt = 10;

    char* which;
    char* hund;
    char* thou;
    char* norm;
    char* twent;
    char* thirt;
    char* fourt;
    char* fifth;
    char* sixt;
    char* sevent;
    char* eighty;
    char* ninet;
    char* eleven;
    char* twelve;
    char* thirteen;
    char* fourteen;
    char* fifteen;
    char* sixteen;
    char* seventeen;
    char* eighteen;
    char* nineteen;

    // ---- better to not initialize variables
    twent = "Twenty";
    thirt = "Thirty";
    fourt = "Forty";
    fifth = "Fifty";
    sixt = "Sixty";
    sevent = "Seventy";
    eighty = "Eighty";
    ninet = "Ninety";
    hund = "Hundred";
    thou = "Thousand";
    norm = "None";
    //--------------
    eleven = "Eleven";
    twelve = "Twelve";
    thirteen = "Thirteen";
    fourteen = "Fourteen";
    fifteen = "Fifteen";
    sixteen = "Sixteen";
    seventeen = "Seventeen";
    eighteen = "Eighteen";
    nineteen = "Nineteen";

    // ----- Variables Sets


    // Initialize }

    printf("\nNote: Numbers 1-9,999 only\n");
    printf("Enter your Number:  ");
    scanf("%i", &UserInput);

    // First if stops everything if false (I know should be with Else, but its good in If.)
    if (UserInput > 9999 || UserInput == 0){

        printf("\nNote: Numbers 1-9,999 only\n");
    }

    // ------------------------------------------------------------

    // only "1" len 1-9
    else if (UserInput <= 10 && UserInput >= 1){

        printf("\n%s\n",MyStringList[UserInput]);
    }
    // contains "2" len 10-99
    else if (UserInput <= 99 && UserInput >= 10){

        if (UserInput >= 20 && UserInput <= 29){ // 20 - 29
            int DecByTen = UserInput - 10;
            //OneIntegral(UserInput);
            if (UserInput == 20){
            	printf("\n%s\n",twent);
            }else{
            printf("\n%s %s\n",twent,MyStringList[UserInput-20]);
            }
        }
        // elif start
        else if (UserInput >= 30 && UserInput <= 39){ // 30 - 39
        	if (UserInput == 30){
        	   printf("\n%s\n",thirt);
        	            }else{
        	            printf("\n%s %s\n",thirt,MyStringList[UserInput-30]);
          }
        } //elif end

        // elif start
        else if (UserInput >= 40 && UserInput <= 49){ // 40 - 49
        	if (UserInput == 40){
        	   printf("\n%s\n",fourt);
        	            }else{
        	            printf("\n%s %s\n",fourt,MyStringList[UserInput-40]);
          }
        } //elif end


        // elif start
        else if (UserInput >= 50 && UserInput <= 59){ // 50 - 59
        	if (UserInput == 50){
        	   printf("\n%s\n",fifth);
        	            }else{
        	            printf("\n%s %s\n",fifth,MyStringList[UserInput-50]);
          }
        } //elif end

        // elif start
        else if (UserInput >= 60 && UserInput <= 69){ // 60 - 69
        	if (UserInput == 60){
        	   printf("\n%s\n",sixt);
        	            }else{
        	            printf("%s %s\n",sixt,MyStringList[UserInput-60]);
          }
        } //elif end

        // elif start
        else if (UserInput >= 70 && UserInput <= 79){ // 70 - 79
        	if (UserInput == 70){
        	   printf("\n%s\n",sevent);
        	            }else{
        	            printf("\n%s %s\n",sevent,MyStringList[UserInput-70]);
          }
        } //elif end

        // elif start
        else if (UserInput >= 80 && UserInput <= 89){ // 80 - 89
        	if (UserInput == 80){
        	   printf("\n%s\n",eighty);
        	            }else{
        	            printf("\n%s %s\n",eighty,MyStringList[UserInput-80]);


          }
        } //elif end

        // elif start
        else if (UserInput >= 90 && UserInput <= 99){ // 90 - 99
        	if (UserInput == 90){
        	   printf("\n%s\n",ninet);
        	            }else{
        	            printf("\n%s %s\n",ninet,MyStringList[UserInput-90]);
          }
        } //elif end
    }


    // contains "3" len 100-999
    else if (UserInput <= 999 && UserInput >= 100){
    	// hundreds

    	int intParsed = 0; //
    	int intSecondParsed[4], letsLoop, intLoop;

    	for(letsLoop=3 ; letsLoop>=0 ; letsLoop--)           //Problem is in the loop
    	    {
    	        intLoop = UserInput % 10;
    	        intSecondParsed[letsLoop] = intLoop;
    	        UserInput /= 10;
    	    }

    	// intSecondParsed[3] // second num
    	if (intSecondParsed[2] == 1){
    	    switch(intSecondParsed[3]){
    	        case 1:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,eleven);
    	            break;
       	        case 2:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,twelve);
    	            break;
       	        case 3:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,thirteen);
    	            break;
       	        case 4:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,fourteen);
    	            break;
       	        case 5:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,fifteen);
    	            break;
       	        case 6:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,sixteen);
    	            break;
       	        case 7:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,seventeen);
                    break;
    	        case 8:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,eighteen);
    	            break;
       	        case 9:
                    printf("\n%s %s %s\n",MyStringList[intSecondParsed[1]],hund,nineteen);
    	            break;

    	    }
    	 }
    	else{
        switch(intSecondParsed[2]){

            case 1:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,twent,MyStringList[intSecondParsed[3]]);
                break;

            case 2:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,twent,MyStringList[intSecondParsed[3]]);
                break;

            case 3:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,thirt,MyStringList[intSecondParsed[3]]);
                break;

            case 4:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,fourt,MyStringList[intSecondParsed[3]]);
                break;

            case 5:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,fifth,MyStringList[intSecondParsed[3]]);
                break;

            case 6:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,sixt,MyStringList[intSecondParsed[3]]);
                break;

            case 7:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,sevent,MyStringList[intSecondParsed[3]]);
                break;

            case 8:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,eighty,MyStringList[intSecondParsed[3]]);
                break;

            case 9:
                printf("\n%s %s %s %s\n",MyStringList[intSecondParsed[1]],hund,ninet,MyStringList[intSecondParsed[3]]);
                break;

        }

    }
}
    // contains "4" len 1000 - 9999
    else if (UserInput <= 9999 && UserInput >= 1000){
    	int intSecondParsed[4], letsLoop, intLoop;

    	for(letsLoop=3 ; letsLoop>=0 ; letsLoop--)           //Problem is in the loop
    	    {
    	        intLoop = UserInput % 10;
    	        intSecondParsed[letsLoop] = intLoop;
    	        UserInput /= 10;
    	    }
    	int f = intSecondParsed[0];
    	int s = intSecondParsed[1];
    	int t = intSecondParsed[2];
    	int l = intSecondParsed[3];
    	switch(t){
    	case 1: {
    		if (l == 1){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,eleven);
    		}

    		else if (l == 2){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,twelve);
    		}
    		else if (l == 3){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,thirteen);

    		}
    		else if (l == 4){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,fourteen);

    		}
    		else if (l == 5){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,fifteen);

    		}
    		else if (l == 6){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,sixteen);

    		}
    		else if (l == 7){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,seventeen);

    		}
    		else if (l == 8){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,eighteen);

    		}

    		else if (l == 9){
    			printf("%s %s %s %s %s    \n",MyStringList[f],thou,MyStringList[s],hund,nineteen);

    		}

    		break;
    	}
    	case 2: {
    		if (l==0){
        		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,twent);

    		}else{
    		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,twent,MyStringList[l]);
    		}
    		break;
    	}
    	case 3: {
    		if (l==0){
        		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,thirt);

    		}else{
    		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,thirt,MyStringList[l]);
    		}
    		break;
    	}
    	case 4: {     		if (l==0){
    		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,fourt);

		}else{
		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,fourt,MyStringList[l]);
		}
		break;
    	}
    	case 5: {
    		if (l==0){
        		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,fifth);

    		}else{
    		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,fifth,MyStringList[l]);
    		}
    		break;
    	}
    	case 6: {     		if (l==0){
    		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,sixt);

		}else{
		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,sixt,MyStringList[l]);
		}
		break;
    	}
    	case 7: {     		if (l==0){
    		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,sevent);

		}else{
		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,sevent,MyStringList[l]);
		}
		break;}
    	case 8: {     		if (l==0){
    		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,eighty);

		}else{
		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,eighty,MyStringList[l]);
		}
		break;}
    	case 9: {     		if (l==0){
    		printf("%s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,ninet);

		}else{
		printf("%s %s %s %s %s %s   \n",MyStringList[f],thou,MyStringList[s],hund,ninet,MyStringList[l]);
		}
		break;}

    	}
    }
}
